FROM python:3.8

RUN apt-get update
RUN apt-get install git

WORKDIR /srv

COPY ./mezzanine ./
COPY ./requirements.txt ./

RUN pip install -r requirements.txt
RUN python /srv/setup.py install
RUN mezzanine-project hillel_docker
RUN python hillel_docker/manage.py createdb --noinput --nodata

CMD ["python", "hillel_docker/manage.py", "runserver", "0:8000"]
